import React from 'react';
import '../App.css';

import UserInfo from './UserInfo';

export default class Comment extends React.Component {
    render() {
        return (
        <div className="Comment">
            <UserInfo user={this.props.author} />
            <div className="Comment-text">
                {this.props.text}
            </div>
            <div className="Comment-date">
                {formatDate(this.props.date)}
            </div>
        </div>
        );
    }
}

function formatDate(date) {
    return date;
}
