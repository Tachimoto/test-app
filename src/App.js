import React from 'react';
// import logo from './logo.svg';
import './App.css';

import Logo192 from './images/logo192.png';
import Comment from './components/Comment';

export default class App extends React.Component {
    render() {
        const addWord = 'alpha';
        const userName = {
            firstName: 'tachimoto',
            lastName: 'kenta',
        };

        const author = {
                avatarUrl: './favicon.ico',
                name: 'avatarName',
        };

        return (
        <div className="App">
            <img src={Logo192} alt="logo192" />
            <h1>Hello, {addWord}!</h1>
            <p>ようこそ {formatName(userName)} さん!</p>
            <p>あなたのニックネームは {this.props.nickName} です。</p>
            <p>10 + 2の計算結果は、「{10 + 2}」です。</p>
            <Comment author={author} text="anyComment" date="anyDate" />
        </div>
        );
    }
}

function formatName(userName) {
    return userName.firstName + ' ' + userName.lastName;
}

/*
function App(props) {
    const addWord = 'alpha';
    const userName = {
        firstName: 'tachimoto',
        lastName: 'kenta',
    };
    return (
    <div className="App">
        <img src={logo192} alt="logo192" />
        <h1>Hello, {addWord}!</h1>
        <p>ようこそ {formatName(userName)} さん!</p>
        <p>あなたのニックネームは {props.nickName} です。</p>
        <p>10 + 2の計算結果は、「{10 + 2}」です。</p>
    </div>
    );
};
*/

/*
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
*/
